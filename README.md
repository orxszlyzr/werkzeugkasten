Toolbox of abundant kinds of calculators
========================================

This project aims at creating a framework and a set of plugins, using 
Javascript as pure as possible, to provide a set of tools able to perform
a lot of calculations.
