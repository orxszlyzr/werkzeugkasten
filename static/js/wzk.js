var wzk = {};

wzk.components = {
    
    physical_quantity: {

        definition: {
            'L': {
                'm': 1000,
                'cm': 10,
                'mm': 1,
                'km': 0.001,
                'inch': 25.4,
            },
            'M': {
                'kg': 1000,
                'g': 1,
                'mg': 0.001,
                'ton': 1,
            },
            'T': {
                's': 86400,
                'min': 1440,
                'day': 1,
                'ms': 86400000,
                'a': 0.00273791,
                'mon': 0.0333333,
            },
        },

        _get_transform_list: function(catalog, unit){
            var test = wzk.components.physical_quantity.definition[catalog][unit];
            if(test == undefined)
                return {unit: 1};
            else
                return wzk.components.physical_quantity.definition[catalog];
        },

        displayer: function(jquery_obj){
            jquery_obj.each(function(){

                var ul = $('<ul>')
                    .addClass('dropdown-menu')
                    .attr('role', 'menu')
                ;

                var _transform_list = wzk.components.physical_quantity._get_transform_list(
                    $(this).data('catalog'),
                    $(this).data('unit')
                );

                for(var unit in _transform_list)
                    ul.append(
                        $('<li>').append(
                            $('<a>', {href: '#'})
                                .text(unit)
                                .click(
                                    {'choosen_unit': unit},
                                    wzk.components.physical_quantity.displayer_click_handler
                                )
                        )
                    );

/*
 
<div class="btn-group">
  <button type="button" class="btn btn-primary">100</button>
  <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
    <span>KiM/h</span>
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="#">km/h</a></li>
    <li><a href="#">m/s</a></li>
  </ul>
</div>
 */
                var base_div = $('<div>')
                    .addClass('btn-group')
                    .append(
                        $('<button>',{name: 'value', type: 'button'})
                            .addClass('btn btn-primary')
                    )
                    .append(
                        $('<button>',{type: 'button'})
                        .append(
                            $('<span>', {name: 'unit'})
                        )
                        .append(
                            $('<span>').addClass('caret')
                        )
                        .addClass('btn btn-primary btn-sm dropdown-toggle')
                        .dropdown()
                    )
                    .append(ul)
                    .data('value', $(this).data('value'))
                    .data('unit', $(this).data('unit'))
                    .data('choosen_unit', $(this).data('unit'))
                    .data('transform_list', _transform_list)
                    .click(wzk.components.physical_quantity.displayer_click_handler)
                    .click()
                ;

                $(this).replaceWith(base_div);
            });
        },

        displayer_click_handler: function(e){
            var target = $(e.target);

            if(e != undefined && e.data != undefined && e.data.choosen_unit != undefined)
                target.data('choosen_unit', e.data.choosen_unit);

            var orig_value = target.data('value');
            var orig_unit = target.data('unit');
            var choosen_unit = target.data('choosen_unit');
            var transform_list = target.data('transform_list');

            if(orig_unit == undefined) return;

            var new_value = orig_value * 1.0 / transform_list[orig_unit] * transform_list[choosen_unit];

            target.find('[name="value"]').text(new_value);
            target.find('[name="unit"]').text(choosen_unit);
        },

    },

};
