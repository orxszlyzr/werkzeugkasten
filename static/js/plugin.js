var plugin = {
    
    path: [],
    
    load: function(base_path, e){ //e comes from config json file.
        var target = $('#navigate_plugin_mode').show();
        plugin.path = base_path; // path of 'index.json', base location of this plugin.

        target.find('[name="name"]').text(e.name);
        target.find('[name="author"]').text(e.meta.author);
        target.find('[name="desc"]').text(e.meta.desc);

        $('#plugin_area').load(
            plugin.path.concat([e.init.html]).join('/'),
            function(){
                $.getScript(
                    plugin.path.concat([e.init.script]).join('/')
                );
            }
        );

        plugin.reload = function(){
            _b = base_path;
            _e = e;
            plugin.unload();
            plugin.load(_b, _e);
        };
        target.find('#plugin_refresh').click(plugin.reload);
    },

    reload: null,

    unload: function(){
        var target = $('#navigate_plugin_mode').hide();
        target.find('#plugin_area')
            .empty()
        ;
        target.find('[name]').text('');
    },

}
