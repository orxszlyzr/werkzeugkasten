var navigate = {
    
    path: [],
    current_type: 'list',
    next_list: [],

    initialize: function(){
        navigate._control.load('plugins');
        $('#navigate_entry').click(navigate.entry_button.click_handler);
        $('#navigate_loading').modal({
            keyboard: false,
            show: false,
        });
    },

    _load_mask: {

        still_loading: false,

        loading: function(){
            navigate._load_mask._normalize();
            navigate._load_mask.still_loading = true;
            setTimeout('navigate._load_mask._delay_show();', 300);
        },

        _delay_show: function(){
            if(navigate._load_mask.still_loading)
                $('#navigate_loading').modal('show');
        },

        error: function(){
            navigate._load_mask.still_loading = false;
            navigate._load_mask._errorize();
            $('#navigate_loading').modal('show');
            setTimeout('navigate._load_mask.success();', 2000);
        },

        success: function(){
            navigate._load_mask.still_loading = false;
            navigate._load_mask._normalize();
            $('#navigate_loading').modal('hide');
        },

        _normalize: function(){
            $('#navigate_loading [name="text"]').text(
                '正在加载，请稍候……'
            );
        },

        _errorize: function(){
            $('#navigate_loading [name="text"]').text(
                '加载错误，可能是网络连接或服务器异常。请稍后刷新重试。'
            );
        },
    },

    _control: {
        
        load: function(next_path){
            navigate._control._load(navigate.path, next_path);
        },

        backward: function(level){
            var orig_path = navigate.path.slice(0, level);
            var next_path = navigate.path[level];
            navigate._control._load(orig_path, next_path);
        },

        _load: function(orig_path, next_path){
            var path = orig_path.concat([next_path, 'index.json']).join('/');
            function success_callback(e){
                path_info = {
                    'orig_path': orig_path,
                    'next_path': next_path,
                };
                navigate._control._load_callback_base(e, path_info);
            };
            navigate._load_mask.loading();
            $.ajax({
                url: path,
                type: 'GET',
                dataType: 'json',
                success: success_callback,
                error: function(){
                    navigate._load_mask.error();
                },
            });
        },

        _load_callback_base: function(e, path_info){
            navigate.path = path_info.orig_path;
            navigate.path.push(path_info.next_path);

            navigate.bar.add(e.name, path_info.orig_path.length);
            navigate._load_mask.success();

            switch(e.type){
                case 'list':
                    $('#navigate_menu_mode').show();
                    navigate.next_list = e.list;
                    navigate.menu.refresh();
                    plugin.unload();
                    break;
                case 'plugin':
                    $('#navigate_menu_mode').hide();
                    plugin.load(navigate.path, e);
                    break;
                default:
                    break;
            }
            navigate.current_type = e.type;
        },

    },

    bar: {
        add: function(name, base_count){
            if(base_count != undefined){
                $('#navigate_bar li').each(function(i,e){
                    if(i >= base_count - 1)
                        $(e).remove();
                });
            }

            $('<li>')
                .data('text', name)
                .data('id', navigate.path.length - 1)
                .appendTo('#navigate_bar')
            ;
            navigate.bar.refresh();
        },

        refresh: function(){
            var target = $('#navigate_bar li');
            var len = target.length;
            target.each(function(i,e){
                if(i == target.length - 1)
                    $(e)
                        .text(
                            $(e).data('text')
                        )
                        .unbind('click', navigate.bar.click_handler)
                    ;
                else
                    $(e)
                        .empty()
                        .append(
                            $('<a>',{
                                'href': '#',
                                'text': $(e).data('text'),
                            })
                        )
                        .click(navigate.bar.click_handler)
                    ;
            });
        },

        click_handler: function(e){
            var li = $(e.target).parent();
            navigate._control.backward(li.data('id'));
        },

    },

    menu: {
        refresh: function(){
            var target = $('#navigate_menu').empty();
            for(var i in navigate.next_list){
                $('<li>')
                    .append(
                        $('<a>',{
                            'text': navigate.next_list[i]['name'],
                            'href': '#',
                        })
                            .data('id', i)
                            .click(navigate.menu.click_handler)
                            .dblclick(navigate.entry_button.click_handler)
                    )
                    .appendTo(target)
                ;
            }

            $('#navigate_default').show();
            $('#navigate_display').hide();
        },

        click_handler: function(e){
            var target = $('#navigate_display').show();
            var id = $(e.target).data('id');
            $('#navigate_default').hide();

            target.children('[data-display]').hide();

            target = target.find(
                '[data-display="' + navigate.current_type + '"]'
            );
            switch(navigate.current_type){
                case 'list':
                    target
                        .show()
                        .find('[name="desc"]')
                            .text(navigate.next_list[id]['desc'])
                    ;
                    break;
                case 'plugin':
                    
                    break;
                default:
                    break;
            }

            $('#navigate_menu li').removeClass('active');
            $(e.target).parent().addClass('active');
            $('#navigate_entry').data('id', id);
        },
    },

    entry_button: {
        
        click_handler: function(e){
            var choosen_id = $(e.target).data('id');
            navigate._control.load(navigate.next_list[choosen_id].path);
        },

    },

};

$(function(){
    navigate.initialize();
});
